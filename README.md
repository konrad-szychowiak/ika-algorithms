# IKA Algorithms

![wersja][1]
![rozmiar repo][3]
![licencja][2]

> **IKA Algorithms** to repozytorium zawierające algorytmy wykorzystywane (i wymagane) na zajęciach klas IKA.

## Wkład

Możesz się włączyć w tworzenie tego repozytorium w następujący sposób:

+ Zgłoszenie błędu
+ Zgłoszenie nieczytelności
+ Dodanie lub poprawienie algorytmu (kodu)
+ Dodanie lub poprawienie komentarzy do kodu
+ Dodanie lub poprawienie opisu
+ Wprowadzenie usprawnień w dokumentacji
+ Wprowadzenie usprawnień w skryptach (generatorach)
+ Prośbę o którekolwiek z powyższych

<!-- links -->

[1]: https://img.shields.io/badge/version-Unreleased-informational.svg?style=for-the-badge
[2]: https://img.shields.io/github/license/konrad-szychowiak/ika-algorithms.svg?style=for-the-badge
[3]: https://img.shields.io/github/repo-size/konrad-szychowiak/ika-algorithms.svg?style=for-the-badge
